import java.io.Serializable;
import java.util.Vector;

public class ResultType implements Serializable
{
    private static final long serialVersionUID = 1L;

    public String description;
    public Vector<Integer> result;
}
import java.io.Serializable;
import java.util.Vector;

public class InputType implements Serializable
{
    private static final long serialVersionUID = 1L;

    public String operation;
    public Vector<Integer> params;

    public InputType(Vector<Integer> params, String operation)
    {
        this.params = params;
        this.operation = operation;
    }
}
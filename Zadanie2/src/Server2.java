import java.rmi.registry.LocateRegistry;

public class Server2
{
    public static void main(String[] args)
    {
        try
        {
            if (LocateRegistry.getRegistry(1099) == null)
                LocateRegistry.createRegistry(1099);

            if (System.getSecurityManager() == null)
                System.setSecurityManager(new SecurityManager());

            IWorker worker = new Worker();
            java.rmi.Naming.rebind("//localhost/calculate", worker);
            //java.rmi.Naming.rebind("//localhost/calculate2", worker);

            System.out.println("Server2 is registered now :-)");
            System.out.println("Press Crl+C to stop...");
        }
        catch (Exception _e)
        {
            _e.printStackTrace();
            return;
        }
    }
}

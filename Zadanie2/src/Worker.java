import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Vector;

public class Worker extends UnicastRemoteObject implements IWorker
{
    private boolean isPrime(int number)
    {
        for (int i = 2; i < number; i++)
        {
            if (number % i == 0)
            {
                return false;
            }
        }
        return true;
    }

    private Vector<Integer> findPrimesBetween(int start, int end)
    {
        Vector<Integer> result = new Vector<>();

        for (int i = start; i <= end; i++)
        {
            if (isPrime(i))
                result.add(i);
        }

        return result;
    }

    public Worker() throws RemoteException
    {
        super();
    }

    @Override
    public ResultType calculate(InputType params) throws RemoteException
    {
        ResultType result = new ResultType();

        if (params.operation == "findPrimesBetween")
        {
            int start = params.params.get(0);
            int end = params.params.get(1);
            result.result = findPrimesBetween(start, end);
            result.description = "Znalezione liczby pierwsze pomiędzy " + start + " i " + end;
        }
        else
        {
            result.description = "Nieprawidłowa operacja";
        }

        return result;
    }
}
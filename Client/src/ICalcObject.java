import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ICalcObject extends Remote
{
    double calculate(double _a, double _b) throws RemoteException;
}
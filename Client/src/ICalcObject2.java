import java.rmi.RemoteException;

public interface ICalcObject2
{
    ResultType calculate(InputType inputParam) throws RemoteException;
}
import javax.xml.transform.Result;

public class Client
{
    public static void main(String[] _args)
    {
        try
        {
            if (System.getSecurityManager() == null)
                System.setSecurityManager(new SecurityManager());

            ICalcObject calcObject = (ICalcObject) java.rmi.Naming.lookup("//localhost/calcObject");
            System.out.println("Pobrano referencje do //localhost/calcObject");

            double result = calcObject.calculate(1.1, 2.2);
            System.out.println("Wynik: " + result);

            ICalcObject2 calcObject2 = (ICalcObject2) java.rmi.Naming.lookup("//localhost/calcObject2");
            System.out.println("Pobrano referencje do //localhost/calcObject2");

            InputType inputObject = new InputType();
            inputObject.x1= 3.4;
            inputObject.x2= 5.5;
            inputObject.operation="add"; //lub "sub"

            ResultType resultObject = calcObject2.calculate(inputObject);
            System.out.println("Wynik2: " + resultObject.result + "/" + resultObject.resultDescription);
        }
        catch (Exception _e)
        {
            _e.printStackTrace();
        }
    }
}
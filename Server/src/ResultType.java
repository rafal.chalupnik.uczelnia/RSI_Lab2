import java.io.Serializable;

public class ResultType implements Serializable
{
    private static final long serialVersionUID = 1L;

    public String resultDescription;
    public double result;
}
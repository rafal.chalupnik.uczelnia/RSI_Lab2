import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class CalcObject2 extends UnicastRemoteObject implements ICalcObject2
{
    public CalcObject2() throws RemoteException
    {
        super();
    }

    public ResultType calculate(InputType _inputParam) throws RemoteException
    {
        ResultType result = new ResultType();
        result.resultDescription = "Operacja: " + _inputParam.operation;

        switch (_inputParam.operation)
        {
            case "add":
                result.result = _inputParam.x1 + _inputParam.x2;
                break;
            case "sub":
                result.result = _inputParam.x1 - _inputParam.x2;
                break;
            default:
                result.result = 0;
                result.resultDescription = "Podano zla operacje";
        }

        return result;
    }
}
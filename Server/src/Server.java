import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Random;

public class Server
{
    public static void main(String[] _args)
    {
        try
        {
            if (LocateRegistry.getRegistry(1099) == null)
                LocateRegistry.createRegistry(1099);

            if (System.getSecurityManager() == null)
                System.setSecurityManager(new SecurityManager());

            CalcObject calcObject = new CalcObject();
            CalcObject2 calcObject2 = new CalcObject2();
            java.rmi.Naming.rebind("//localhost/calcObject", calcObject);
            java.rmi.Naming.rebind("//localhost/calcObject2", calcObject2);

            System.out.println("Server is registered now :-)");
            System.out.println("Press Crl+C to stop...");
        }
        catch (Exception _e)
        {
            _e.printStackTrace();
            return;
        }
    }
}
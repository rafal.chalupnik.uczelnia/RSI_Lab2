import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class CalcObject extends UnicastRemoteObject implements ICalcObject
{
    public CalcObject() throws RemoteException
    {
        super();
    }

    @Override
    public double calculate(double _a, double _b) throws RemoteException
    {
        return _a + _b;
    }
}
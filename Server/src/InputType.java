import java.io.Serializable;

public class InputType implements Serializable
{
    private static final long serialVersionUID = 1L;

    public String operation;
    public double x1;
    public double x2;
}
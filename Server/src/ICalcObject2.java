import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ICalcObject2 extends Remote
{
    ResultType calculate(InputType _inputParam) throws RemoteException;
}